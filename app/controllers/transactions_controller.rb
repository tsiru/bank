class TransactionsController < ApplicationController
  before_action { authorize :transactions, :manage_own? }
  before_action :set_transaction_form, only: [:new, :create]

  def index
    @transactions = TransactionPresenter.for_account(account)
    respond_with @transactions
  end

  def new
    respond_with @transaction_form
  end

  def create
    @transaction_form.create(transaction_params)
    respond_with @transaction_form, location: transactions_path
  end

  private

  def transaction_params
    params.require(:transaction_form).permit(:title, :receiver_id, :amount)
  end

  def set_transaction_form
    @transaction_form = TransactionForm.new account
  end

  def account
    @account ||= current_user.account
  end
end
