module Admin
  class TransactionsController < ApplicationController
    before_action { authorize :transactions, :preview_all? }

    def index
      @transactions = Transaction.all
      respond_with @transactions
    end
  end
end
