class TransactionPresenter < Struct.new(:account, :transaction)
  def self.for_account(account)
    account.transactions.map do |transaction|
      new account, transaction
    end
  end

  delegate :created_at, :title, to: :transaction

  def incoming?
    transaction.target_account == account
  end

  def amount
    incoming? ? transaction.amount : -transaction.amount
  end

  def from
    (incoming? ? transaction.charged_account : transaction.target_account).user.email
  end
end
