class TransactionsPolicy < ApplicationPolicy
  def manage_own?
    user && !user.account.internal
  end

  def preview_all?
    user && user.account.internal
  end
end
