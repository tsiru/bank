class Account < ActiveRecord::Base
  class BallanceToLow < StandardError; end
  class ConsistencyError < StandardError; end

  scope :internal, -> { where(internal: true) }
  scope :non_internal, -> { where(internal: false) }

  validates :user, presence: true

  has_many :transactions_made,
    foreign_key: :charged_account_id,
    class_name: 'Transaction',
    inverse_of: :charged_account

  has_many :transactions_received,
    foreign_key: :target_account_id,
    class_name: 'Transaction',
    inverse_of: :target_account

  belongs_to :user

  before_save :check_negative_ballance!, unless: :internal
  before_save :check_transactions_consistency!

  def transactions
    (transactions_received + transactions_made).sort_by(&:created_at)
  end

  def transfer(amount, target_account, title: 'standard transaction')
    transaction do
      self.transactions_made.create!(
        charged_account: self,
        target_account: target_account,
        title: title,
        charged_account_ballance_before: self.ballance,
        target_account_ballance_before: target_account.ballance,
        amount: amount,
      )

      self.ballance -= amount
      target_account.ballance += amount

      self.save!
      target_account.save!
    end
    true
  end

  private

  def check_transactions_consistency!
    diff = transactions_received.pluck(:amount).sum - transactions_made.pluck(:amount).sum - ballance
    raise ConsistencyError, 'Ballance does not match transaction history' unless diff.zero?
  end

  def check_negative_ballance!
    if ballance < 0
      raise BallanceToLow, 'Unable to transfer money'
    end
  end
end
