class Transaction < ActiveRecord::Base
  validates :charged_account, :target_account,
    :charged_account_ballance_before, :target_account_ballance_before,
    :amount, :title, presence: true

  belongs_to :charged_account,
    class_name: 'Account',
    inverse_of: :transactions_made

  belongs_to :target_account,
    class_name: 'Account',
    inverse_of: :transactions_received

end
