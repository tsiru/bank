class TransactionForm < Struct.new(:account)
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :title, :receiver_id, :amount
  validates :title, :amount, :receiver_id, presence: true

  def receivers
    @receivers = Account
      .joins(:user)
      .non_internal
      .where('accounts.id != ?', account.id)
      .group('accounts.user_id')
      .map { |account| [account.user.email, account.id] }
  end

  def persisted?
    false
  end

  def create(attributes)
    self.attributes = attributes
    account.transfer(Float(amount), receivers_account, title: title) if valid?
  end

  def attributes=(attributes)
    attributes.each do |k,v|
      public_send("#{k}=", v)
    end
    self
  end

  private

  def receivers_account
    Account.find(receiver_id)
  end
end
