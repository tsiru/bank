puts "seeding ..."

User.create!(
  email: 'admin@localhost',
  password: 'password'
).create_account!(internal: true)

User.create!(
  email: 'user1@localhost',
  password: 'password',
).create_account!

User.create!(
  email: 'user2@localhost',
  password: 'password',
).create_account!

puts "seading done!"
