class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.references :user, index: true, foreign_key: true, notnull: true, unique: true
      t.decimal :ballance, precision: 8, scale: 2, default: 0, notnull: true
      t.boolean :internal, default: false, notnull: true

      t.timestamps null: false
    end
  end
end
