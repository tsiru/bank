class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :charged_account, notnull: true
      t.references :target_account, notnull: true

      t.string :title

      t.decimal :charged_account_ballance_before, precision: 8, scale: 2, notnull: true
      t.decimal :target_account_ballance_before,  precision: 8, scale: 2, notnull: true
      t.decimal :amount, precision: 8, scale: 2, notnull: true

      t.timestamps null: false
    end
  end
end
