RSpec.describe "transactions" do
  context 'when user exists' do
    let!(:account) { create :account, ballance: 1000 }
    let!(:receivers_account) { create :account }

    before { login account.user }

    subject do
      -> {
        visit '/'
        click_link 'Transactions'
        click_link 'New'

        fill_in 'Amount', with: 10
        fill_in 'Title', with: 'test'
        select receivers_account.user.email, from: "Receiver"
        click_button 'Do Transaction'
      }
    end

    it { is_expected.to change { account.reload.ballance }.from(1000).to(990) }
    it { is_expected.to change { receivers_account.reload.ballance }.by(10) }

  end
end
