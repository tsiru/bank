RSpec.describe "users:create EMAIL=user@example.com PASSWORD=password" do
  include_context "rake"

  it 'should create user with given email and password' do
    expect(User.find_by(email: 'user@example.com')).to be_nil
    subject.invoke
    user = User.find_by(email: 'user@example.com')
    expect(user).to be_present
    expect(user.account).to be_present
  end
end
