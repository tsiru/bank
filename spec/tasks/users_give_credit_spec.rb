RSpec.describe "users:give_credit EMAIL=user@example.com AMOUNT=40" do
  include_context "rake"

  let!(:internal_account) { create(:account, :internal) }
  let!(:user) { create(:user, :with_account, email: 'user@example.com') }
  it 'should create user with given email and password' do
    expect {
      subject.invoke
    }.to change {
      user.account.reload.ballance
    }.from(0).to(40)

    expect(user.account.transactions_received.last.title).to eq 'CREDIT'
    expect(internal_account.transactions_made.last.title).to eq 'CREDIT'
  end
end
