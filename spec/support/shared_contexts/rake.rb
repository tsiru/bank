require "rake"

RSpec.shared_context "rake" do
  let(:rake)        { Rake::Application.new }
  let(:task_name)   { self.class.top_level_description.split(' ').first }
  let(:task_params) { self.class.top_level_description.split(' ').grep(/^([A-Z_]+)=(.*)$/).map{ |param| param.split('=') }.to_h }
  let(:task_path)   { "lib/tasks/#{task_name.split(":").first}" }
  subject           { rake[task_name] }

  def loaded_files_excluding_current_rake_file
    $".reject {|file| file == Rails.root.join("#{task_path}.rake").to_s }
  end

  before do
    Rake.application = rake
    Rake.application.rake_require(task_path, [Rails.root.to_s], loaded_files_excluding_current_rake_file)

    Rake::Task.define_task(:environment)
  end

  around do |block|
    begin
      prev_env_variables = task_params.map { |k,v| [k, ENV[v]] }.to_h
      task_params.each { |k,v| ENV[k] = v }
      block.call
    ensure
      prev_env_variables.each { |k,v| ENV[k] = v }
    end
  end
end
