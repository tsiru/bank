FactoryGirl.define do
  factory :user do
    email    { Faker::Internet.email }
    password { Faker::Lorem::characters(10) }

    trait :with_account do
      account
    end
  end
end
