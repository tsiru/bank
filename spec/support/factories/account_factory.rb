FactoryGirl.define do
  factory :account do
    user

    transient do
      ballance { 0 }
    end

    trait :internal do
      internal { true }
    end

    after(:create) do |account, options|
      if options.ballance != 0
        create(:account, :internal).transfer(options.ballance, account)
      end
    end
  end
end
