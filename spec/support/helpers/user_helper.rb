module UserHelper
  def login(user)
    visit '/'
    expect(page).to have_content 'You need to sign in or sign up before continuing.'
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
    expect(page).to have_content "Signed in successfully."
  end
end
