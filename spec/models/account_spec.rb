require 'rails_helper'

RSpec.describe Account, type: :model do
  it { is_expected.to have_many(:transactions_made) }
  it { is_expected.to have_many(:transactions_received) }
  it { is_expected.to belong_to(:user) }

  it { is_expected.to validate_presence_of(:user) }

  let!(:charged_account) { create(:account, ballance: 200) }
  let!(:target_account) { create(:account) }
  let(:amount) { 100 }

  describe '#transactions' do
    before { charged_account.transfer(amount, target_account) }
    before { target_account.transfer(amount, charged_account) }

    let(:combined_transactions) { charged_account.transactions_made + charged_account.transactions_received }

    it 'transaction from and to account are visible under #transactions' do
      expect(charged_account.transactions.sort).to eq(combined_transactions.sort)
    end
  end

  describe '#transfer' do
    subject { -> { charged_account.transfer(amount, target_account) } }

    context 'transfers many from one account to another' do
      it { is_expected.to change(charged_account, :ballance).by(-amount) }
      it { is_expected.to change(target_account, :ballance).by(amount) }

      it 'creates transaction history for charged account' do
        is_expected.to change(charged_account.transactions_made, :count).by(1)
        is_expected.to_not change(charged_account.transactions_received, :count)
        transaction = charged_account.transactions_made.last
        expect(transaction.charged_account_ballance_before).to eq(charged_account.ballance+amount)
      end

      it 'creates transaction history for target account' do
        is_expected.to change(target_account.transactions_received, :count).by(1)
        is_expected.to_not change(target_account.transactions_made, :count)
        transaction = target_account.transactions_received.last
        expect(transaction.target_account_ballance_before).to eq(target_account.ballance-amount)
      end

      context 'when fail occour during save' do
        before { expect_any_instance_of(described_class).to receive(:valid?).and_return(false) }

        it 'leaves everything like it was before' do
          data_before = {
            transactions_count: Transaction.count,
            charged_account_ballance: charged_account.ballance,
            target_account_ballance: target_account.ballance,
          }

          is_expected.to raise_error(ActiveRecord::RecordInvalid)

          data_after = {
            transactions_count: Transaction.count,
            charged_account_ballance: charged_account.reload.ballance,
            target_account_ballance: target_account.reload.ballance,
          }

          expect(data_before).to eq data_after
        end
      end

      context 'when account hasn\'t enough ballance' do
        let(:charged_account) { create(:account, ballance: 0) }

        it { is_expected.to raise_error('Unable to transfer money', Account::BallanceToLow) }
      end

      context 'when changing ballance manually' do
        it 'prevents to save data' do
          expect {
            charged_account.ballance += 100
            charged_account.save!
          }.to raise_error('Ballance does not match transaction history', Account::ConsistencyError)
        end
      end
    end
  end

  def supress_exception(klass)
    yield
  rescue klass
    nil
  end
end
