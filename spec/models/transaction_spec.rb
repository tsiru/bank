require 'rails_helper'

RSpec.describe Transaction, type: :model do
  it { is_expected.to belong_to(:charged_account) }
  it { is_expected.to belong_to(:target_account) }

  it { is_expected.to validate_presence_of(:charged_account) }
  it { is_expected.to validate_presence_of(:target_account) }
  it { is_expected.to validate_presence_of(:amount) }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:charged_account_ballance_before) }
  it { is_expected.to validate_presence_of(:target_account_ballance_before) }
end
