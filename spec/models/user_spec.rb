require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_one(:account) }
  it { is_expected.to validate_presence_of :email }
  it { is_expected.to validate_presence_of :password }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  it { expect(create(:user).account).to_not be_present }
end
