# Configuration #

`bundle install`

# Database creation #

Application uses SQLite. Please run `rake db:setup`

# How to run the test suite #

Type `rspec`

## First usage ##

After initialization there are provided accounts for tree users:

* admin@localhost
* user1@localhost
* user2@localhost

password for each account is 'password'

Admin has ability to give users a credit. To give uer a credit from command line run command `rake users:give_credit EMAIL=user@example.com AMOUNT=40` where amount is amount of money for user with given email.

## Users creation ##

To create user use command `rake users:create EMAIL=user@example.com PASSWORD=password`. Account for user will be automaticly assiged.

## TODO ##

* Tests for permissions and policies. Improve test coverage in general.
* More friendly GUI