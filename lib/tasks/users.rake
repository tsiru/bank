namespace :users do
  desc "Creates user with given email and password"
  task create: :environment do
    email = ENV['EMAIL'].presence or raise 'missing environment variable EMAIL'
    password = ENV['PASSWORD'].presence or raise 'missing environment variable PASSWORD'

    User.create!(email: email, password: password).create_account!
  end

  desc "Adds user a credit"
  task give_credit: :environment do
    email = ENV['EMAIL'].presence or raise 'missing environment variable EMAIL'
    amount = Float(ENV['AMOUNT']) or raise 'missing environment variable AMOUNT'

    target_account = User.find_by(email: email).account
    source_account = Account.internal.first!

    source_account.transfer(amount, target_account, title: 'CREDIT')
  end

end
